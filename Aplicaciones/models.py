from django.db import models

# Create your models here.

class Cliente(models.Model):
    nombre = models.CharField(max_length=20)
    apellidos = models.CharField(max_length=30)
    dni = models.CharField(max_length=9, unique=True)
    direccion = models.CharField(max_length=40)
    fechanacimiento =models.DateField()
    edad=models.IntegerField()

    def __str__(self):
        return self.nombre
    
class Producto(models.Model):
    nombre = models.CharField(max_length=10)
    codigo = models.CharField(max_length=5)
    

    def __str__(self):
        return self.nombre

class Proveedor(models.Model):
    nombre = models.CharField(max_length=100)
    direccion = models.CharField(max_length=200)
    correo = models.EmailField()
    telefono = models.CharField(max_length=20, blank=True, null=True)

    def __str__(self):
        return self.nombre
    
class Telefono(models.Model):
    dni = models.CharField(max_length=9)
    telefono = models.CharField(max_length=10)
    propietario = models.ForeignKey('Cliente', on_delete=models.CASCADE, related_name='telefonos_clientes', null=True, blank=True)
    proveedor = models.ForeignKey('Proveedor', on_delete=models.CASCADE, related_name='telefonos_proveedores', null=True, blank=True)

    def __str__(self):
        return self.numero

